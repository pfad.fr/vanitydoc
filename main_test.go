// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

package main

import (
	"context"
	"io/fs"
	"net/http"
	"os"
	"path/filepath"
	"testing"
	"time"

	"code.pfad.fr/vanitydoc/autodiscovery"
	"code.pfad.fr/vanitydoc/template"
)

var testStylesheets = []string{"/reseter.min.css", "/style.css"}

func run(fsys fs.FS, modulePath string, htmlOptions htmlOptions, outputFolderGmi string, vcs autodiscovery.VCS) error {
	gopkgs, err := pkgFromRoot(fsys, modulePath)
	if err != nil {
		return err
	}
	return generate(gopkgs, htmlOptions, gemtextOptions{
		OutputFolder: outputFolderGmi,
	}, vcs, vcs.DefaultBranch)
}

func TestGenerateNested(t *testing.T) {
	examplePath, err := filepath.Abs("example")
	if err != nil {
		t.Fatal(err)
	}
	modDir := filepath.Join(examplePath, "nested")
	output := filepath.Join(examplePath, "testdata", "nested")
	err = run(os.DirFS(modDir), "example.com/nested", htmlOptions{
		OutputFolder: output,
		renderer: template.HTMLRenderer{
			Stylesheets: testStylesheets,
			BaseURL:     "/nested@v1",
		},
	}, output, autodiscovery.SourceHutGit("git.example.com/~username/nested", "main"))
	if err != nil {
		t.Fatal(err)
	}
}

func TestGenerateEmptyModule(t *testing.T) {
	examplePath, err := filepath.Abs("example")
	if err != nil {
		t.Fatal(err)
	}
	modDir := filepath.Join(examplePath, "empty-module")
	output := filepath.Join(examplePath, "testdata", "empty-module")
	err = run(os.DirFS(modDir), "example.com/empty-module", htmlOptions{
		OutputFolder: output,
		renderer: template.HTMLRenderer{
			Stylesheets: testStylesheets,
		},
	}, output, autodiscovery.SourceHutGit("git.example.com/~username/nested", "main"))
	if err != nil {
		t.Fatal(err)
	}
}

func TestServe(t *testing.T) {
	t.Run("default arguments", testServe("", "", htmlRendererFlags{}))
	t.Run("folder path", testServe("", ".", htmlRendererFlags{}))
}

func testServe(modulePath, jsonPath string, htmlFlags htmlRendererFlags) func(*testing.T) {
	return func(t *testing.T) {
		timeout := 3 * time.Second
		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		t.Cleanup(cancel)

		mainErr := make(chan error)
		go func() {
			mainErr <- mainWithFlags(ctx, modulePath, jsonPath, htmlFlags, ":8765", "")
		}()

		// wait for the server to start
		for {
			_, err := http.Get("http://localhost:8765")
			if err == nil {
				break
			} else if ctx.Err() != nil {
				t.Fatalf("no reply from server after %s", timeout)
			} else {
				time.Sleep(time.Millisecond)
			}
		}

		cancel()
		if err := <-mainErr; err != nil {
			t.Fatal(err)
		}
	}
}
