// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

// command is doing some stuff
//
// It is the occasion for adding
//   - link to other domain with anchor [code.pfad.fr/vanitydoc/autodiscovery.Forge]
//   - link to other domain without anchor [code.pfad.fr/vanitydoc/autodiscovery]
//   - link to same domain with anchor [example.com/empty-module/pkg.NonExisting]
//   - link to same domain without anchor [example.com/empty-module/pkg]
package main
