// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

// Sub package
package sub

type Int int
type Marine struct{ Depth, Speed int }

type Color interface {
	Yellow()         // jaune
	WithArg(int) int // argh
	WithArgName(color string) (n int, err error)
	WithArgName2(color string) (n int,
		err error)
	WithArgName3(color string) (
		int,
		err error,
	)
}

type ColorGen[T int, U,
	V string,
	WW int, X float64] interface{ WithArg(T) }

type ColorGen2[
	T, U interface{ WithArg(T) },
	V string,
	WW int, X float64,
] interface{ WithArg(T) }

// const documentation
const (
	// Type mismatch errors.
	ERR_CONTENT_TYPE = "ContentTypeError"

	// Top comment
	// on multiple lines
	// wi	<strong>HTML</strong>
	ERR_REQUIRED       = "RequiredError"              // trailing comment
	ERR_ALPHA_DASH_DOT = "<strong>	Dot</strong>Error" // trailing comment

	ERR_SPACED = "RequiredError"

	SOME_STRING  string = "1"
	OTHER_STRING string = "2"
)

var (
	// single value
	ANSWER = &Marine{1, 42}
)

var One, Two Int = 1, 2

func Generic[T Color](submarie T) {

}
