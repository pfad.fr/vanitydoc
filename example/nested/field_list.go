// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

package nested

type InterfaceInline interface{ Foo(int, string) }
type InterfaceMultiline interface {
	Foo(
		int,
		string,
	)
	Foor(
		int,
		int)
	Bar() error
	Barr() (n int, err error)
	unexported()
}

type StructInline struct{ A int }
type StructMultiline[T any] struct {
	A int
	B func(T)
}

var FunInline = FuncGen(1, 2)
var FunMultiline = FuncGen(
	1, // one
	2, // two
)

var CompositeLit = append([]int{
	1,
	2,
}, 1, 2,
	3)

var CallExpr = append([]int{
	1,
	2,
}, []int{1, 2}...)

var CallExprNewLine = append([]int{
	1,
	2,
}, []int{
	1,
	2,
}...,
)

var Unexported = struct{ unexported int }{unexported: 1}

type MapFunc map[string]func(int)

func (MapFunc) RegisterOnShutdown(f func()) {}
