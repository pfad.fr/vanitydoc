// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

// All the way down
package turtle

type Typ struct{}

// Method has an associated example.
func (t Typ) Method() int {
	return 42
}
