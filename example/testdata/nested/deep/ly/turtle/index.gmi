# turtle package - example.com/nested
```
import "example.com/nested/deep/ly/turtle"
```

All the way down

## Types

### type Typ
```
type Typ struct{}
```

### func (Typ) Method
```
func (t Typ) Method() int
```
Method has an associated example.

> Example
```
fmt.Println("Answer:", Typ{}.Method())
```


## Files

=> https://git.example.com/~username/nested/tree/main/item/deep/ly/turtle/turtle.go turtle.go

## Breadcrumb
=> /nested/deep/ly/ example.com/nested/deep/ly
=> /nested/ example.com/nested

