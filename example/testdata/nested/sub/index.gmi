# sub package - example.com/nested
```
import "example.com/nested/sub"
```

Sub package

## Constants

```
const (
	// Type mismatch errors.
	ERR_CONTENT_TYPE = "ContentTypeError"

	// Top comment
	// on multiple lines
	// wi	<strong>HTML</strong>
	ERR_REQUIRED       = "RequiredError"              // trailing comment
	ERR_ALPHA_DASH_DOT = "<strong>	Dot</strong>Error" // trailing comment

	ERR_SPACED = "RequiredError"

	SOME_STRING  string = "1"
	OTHER_STRING string = "2"
)
```
const documentation

## Variables

```
var (
	// single value
	ANSWER = &Marine{1, 42}
)
```

## Functions

### func Generic
```
func Generic[T Color](submarie T)
```

## Types

### type Color
```
type Color interface {
	Yellow()         // jaune
	WithArg(int) int // argh
	WithArgName(color string) (n int, err error)
	WithArgName2(color string) (n int,
		err error)
	WithArgName3(color string) (
		int,
		err error,
	)
}
```

### type ColorGen
```
type ColorGen[T int, U,
	V string,
	WW int, X float64] interface{ WithArg(T) }
```

### type ColorGen2
```
type ColorGen2[
	T, U interface{ WithArg(T) },
	V string,
	WW int, X float64,
] interface{ WithArg(T) }
```

### type Int
```
type Int int
```

```
var One, Two Int = 1, 2
```

### type Marine
```
type Marine struct{ Depth, Speed int }
```

## Files

=> https://git.example.com/~username/nested/tree/main/item/sub/sub.go sub.go

## Breadcrumb
=> /nested/ example.com/nested

