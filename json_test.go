// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

package main

import (
	"archive/tar"
	"archive/zip"
	"bytes"
	"compress/gzip"
	"context"
	"io/fs"
	"net"
	"net/http"
	"os"
	"strings"
	"sync/atomic"
	"testing"
	"time"

	"code.pfad.fr/vanitydoc/autodiscovery"
)

func TestJSON(t *testing.T) {
	err := fromJSON(strings.NewReader(`{}`))
	if err != nil {
		t.Fatal(err)
	}

	ln, err := net.Listen("tcp", "localhost:1234")
	if err != nil {
		t.Skipf("could not listen to localhost:1234: %v", err)
	}
	t.Cleanup(func() { ln.Close() })

	archive, err := os.Open("example/godoc-tricks-1.5.0.zip")
	if err != nil {
		t.Errorf("could not open: %v", err)
	}
	t.Cleanup(func() { archive.Close() })

	var served atomic.Bool
	s := &http.Server{
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			got, want := "/archive.zip", r.URL.Path
			if got != want {
				t.Errorf("request got %q, want %q", got, want)
			}
			http.ServeContent(w, r, "archive.zip", time.Time{}, archive)
			served.Store(true)
		}),
	}
	go s.Serve(ln)

	err = mainWithFlags(context.Background(), "", "example/godoc-tricks.json", htmlRendererFlags{}, "", "")
	if err != nil {
		t.Fatal(err)
	}
	if !served.Load() {
		t.Error("file was not served")
	}
}

func TestRemoveVersionSuffix(t *testing.T) {
	for input, expected := range map[string]string{
		"simple":        "simple",
		"simple/v":      "simple/v",
		"simple/v1":     "simple/v1",
		"simple/v2":     "simple",
		"simple/v-2":    "simple/v-2",
		"simple/vv2":    "simple/vv2",
		"simple/v2v":    "simple/v2v",
		"multi/vath/v2": "multi/vath",
		"multi/vath":    "multi/vath",
	} {
		t.Run(input, func(t *testing.T) {
			got := removeVersionSuffix(input)
			if got != expected {
				t.Errorf("expected %q, got: %q", expected, got)
			}
		})
	}
}

func TestArchiveURL(t *testing.T) {
	t.Run("github", func(t *testing.T) {
		url, isTarGz := archiveURL("github", "https://github.com/fluhus/godoc-tricks", "heads/master")
		expected := "https://github.com/fluhus/godoc-tricks/archive/refs/heads/master.zip"
		if url != expected || isTarGz {
			t.Errorf("expected=%q, got=%q %v", expected, url, isTarGz)
		}
	})
	t.Run("forgejo", func(t *testing.T) {
		url, isTarGz := archiveURL("forgejo", "https://forgejo.example.com/org/repo", "main")
		expected := "https://forgejo.example.com/org/repo/archive/main.zip"
		if url != expected || isTarGz {
			t.Errorf("expected=%q, got=%q %v", expected, url, isTarGz)
		}
	})
	t.Run("gitsrht", func(t *testing.T) {
		url, isTarGz := archiveURL("gitsrht", "https://git.example.com/org/repo", "main")
		expected := "https://git.example.com/org/repo/archive/main.tar.gz"
		if url != expected || !isTarGz {
			t.Errorf("expected=%q, got=%q %v", expected, url, isTarGz)
		}
	})
	t.Run("hgsrht", func(t *testing.T) {
		url, isTarGz := archiveURL("hgsrht", "https://hg.example.com/org/repo", "main")
		expected := "https://hg.example.com/org/repo/archive/main.tar.gz"
		if url != expected || !isTarGz {
			t.Errorf("expected=%q, got=%q %v", expected, url, isTarGz)
		}
	})
}

func TestPopulateArchiveURL(t *testing.T) {
	t.Run("github", func(t *testing.T) {
		m := module{vcs: autodiscovery.Github("https://github.com/fluhus/godoc-tricks", "master")}
		m.populateArchiveURL(moduleDefaults{
			Type:             "github",
			ArchiveRefPrefix: "heads/",
		})
		expected := "https://github.com/fluhus/godoc-tricks/archive/refs/heads/master.zip"
		if m.ArchiveURL != expected || m.ArchiveIsTarGz {
			t.Errorf("expected=%q, got=%q %v", expected, m.ArchiveURL, m.ArchiveIsTarGz)
		}
	})
	t.Run("forgejo", func(t *testing.T) {
		m := module{vcs: autodiscovery.Forgejo("https://forgejo.example.com/org/repo", "main")}
		m.populateArchiveURL(moduleDefaults{
			Type: "forgejo",
		})
		expected := "https://forgejo.example.com/org/repo/archive/main.zip"
		if m.ArchiveURL != expected || m.ArchiveIsTarGz {
			t.Errorf("expected=%q, got=%q %v", expected, m.ArchiveURL, m.ArchiveIsTarGz)
		}
	})
	t.Run("gitsrht", func(t *testing.T) {
		m := module{vcs: autodiscovery.SourceHutGit("https://git.example.com/org/repo", "main")}
		m.populateArchiveURL(moduleDefaults{
			Type: "gitsrht",
		})
		expected := "https://git.example.com/org/repo/archive/main.tar.gz"
		if m.ArchiveURL != expected || !m.ArchiveIsTarGz {
			t.Errorf("expected=%q, got=%q %v", expected, m.ArchiveURL, m.ArchiveIsTarGz)
		}
	})
	t.Run("hgsrht", func(t *testing.T) {
		m := module{vcs: autodiscovery.SourceHutHg("https://hg.example.com/org/repo", "main")}
		m.populateArchiveURL(moduleDefaults{
			Type: "hgsrht",
		})
		expected := "https://hg.example.com/org/repo/archive/main.tar.gz"
		if m.ArchiveURL != expected || !m.ArchiveIsTarGz {
			t.Errorf("expected=%q, got=%q %v", expected, m.ArchiveURL, m.ArchiveIsTarGz)
		}
	})
}

func TestTarGzToZip(t *testing.T) {
	var src bytes.Buffer
	srcg := gzip.NewWriter(&src)
	srct := tar.NewWriter(srcg)
	if err := srct.AddFS(os.DirFS("example/empty-module")); err != nil {
		t.Fatal(err)
	}
	if err := srct.Close(); err != nil {
		t.Fatal(err)
	}
	if err := srcg.Close(); err != nil {
		t.Fatal(err)
	}

	var dst bytes.Buffer
	if err := tarGzipToZip(&dst, &src); err != nil {
		t.Fatal(err)
	}
	dstr := bytes.NewReader(dst.Bytes())
	dstz, err := zip.NewReader(dstr, int64(dstr.Len()))
	if err != nil {
		t.Fatal(err)
	}
	buf, err := fs.ReadFile(dstz, "go.mod")
	if err != nil {
		t.Fatal(err)
	}
	if expected := 151; len(buf) != expected {
		t.Fatalf("expected go.mod len=%d, got=%d", expected, len(buf))
	}
}

func TestFirstNonEmpty(t *testing.T) {
	if firstNonEmpty() != "" {
		t.Fatal("unexpected firstNonEmpty")
	}
	if firstNonEmpty("a") != "a" {
		t.Fatal("unexpected firstNonEmpty")
	}
	if firstNonEmpty("", "a") != "a" {
		t.Fatal("unexpected firstNonEmpty")
	}
	if firstNonEmpty("a", "") != "a" {
		t.Fatal("unexpected firstNonEmpty")
	}
	if firstNonEmpty("a", "b") != "a" {
		t.Fatal("unexpected firstNonEmpty")
	}
}
