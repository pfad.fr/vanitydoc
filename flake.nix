# SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
#
# SPDX-License-Identifier: CC0-1.0

{
  description = "generate go documentation as a static site";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";

    devf.url = "git+https://code.pfad.fr/devf"; # append ?ref=refs/tags/v0.8.0 to specify the tag
  };

  outputs = { self, nixpkgs, flake-utils, devf }:
    flake-utils.lib.eachDefaultSystem (system:
      let

        pkgs = nixpkgs.legacyPackages.${system};

        # to work with older version of flakes
        lastModifiedDate = self.lastModifiedDate or self.lastModified or "19700101";

        # Generate a user-friendly version number.
        version = builtins.substring 0 8 lastModifiedDate;

        devf-cli = devf.packages.${system}.default;
      in
      {
        # See https://ryantm.github.io/nixpkgs/languages-frameworks/go/
        packages.default = pkgs.buildGoModule {
          name = "vanitydoc";
          inherit version;
          src = ./.;
          # manually list subpackages to prevent building the go packages under "example"
          subPackages = [ "." ];
          vendorHash = null;
          doCheck = false;
        };
        devShells.default = pkgs.mkShell {
          packages = [ devf-cli ];
          buildInputs = with pkgs; [ go go-tools reuse inotify-tools ];
        };
        formatter = pkgs.nixpkgs-fmt;
      });
}
