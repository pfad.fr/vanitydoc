// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

package main

import (
	"archive/zip"
	"context"
	_ "embed"
	"encoding/json"
	"errors"
	"fmt"
	"html"
	"io"
	"io/fs"
	"log"
	"log/slog"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"

	"code.pfad.fr/vanitydoc/autodiscovery"
	"code.pfad.fr/vanitydoc/template"
)

func serve(ctx context.Context, fsys fs.FS, modulePath string, htmlFlags htmlRendererFlags, addr, gomodcache string) error {
	s := server{
		fsys:       fsys,
		modulePath: modulePath,
		renderer:   htmlFlags.renderer("", ""),
		// vcs:        vcs,
	}
	if gomodcache != "" {
		s.gomodcache = os.DirFS(gomodcache)
	}

	start := time.Now()
	maxDelay := 300 * time.Millisecond
	if _, err := pkgFromRoot(s.fsys, s.modulePath); err != nil || time.Since(start) > maxDelay {
		s.useFallback = true
	}

	var ln net.Listener
	var err error
	if fdNumber, useFD := strings.CutPrefix(addr, "fd:"); useFD {
		ln, err = openFDListener(fdNumber)
	} else {
		ln, err = net.Listen("tcp", addr)
	}
	if err != nil {
		return err
	}
	defer ln.Close()
	log.Printf("URL: http://%s\n", ln.Addr().String())

	// overwrite server.is_running file (if it exists) to signal that the server is listening
	if f, err := os.OpenFile("server.is_running", os.O_RDWR|os.O_TRUNC, 0o666); err == nil {
		f.Write([]byte(ln.Addr().String()))
		f.Close()
	}

	go http.Serve(ln, s)

	<-ctx.Done()
	return nil
}

func openFDListener(fdString string) (net.Listener, error) {
	fd, err := strconv.ParseUint(fdString, 10, 64)
	if err != nil {
		return nil, fmt.Errorf("'%s' is a malformed file descriptor", fdString)
	}

	file := os.NewFile(uintptr(fd), fdString)
	defer file.Close() // can be closed as soon as net.FileListener was called

	return net.FileListener(file)
}

type server struct {
	fsys        fs.FS
	modulePath  string
	renderer    template.HTMLRenderer
	useFallback bool
	gomodcache  fs.FS

	vcs autodiscovery.VCS // unset (for now)
	ref string            // unset (for now)
}

//go:embed template/vanitydoc-based-on-gopherize.me.png
var gopher []byte

// ServeHTTP implements http.Handler.
func (s server) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(rw, "method not allowed", http.StatusMethodNotAllowed)
		return
	}
	switch r.URL.Path {
	case "/gopher.png":
		rw.Header().Set("Content-Type", "image/png")
		rw.Write(gopher)
		return
	case "/favicon.ico":
		http.NotFound(rw, r)
		return
	case "/vanitydoc.css":
		rw.Header().Set("Content-Type", "text/css; charset=utf-8")
		err := template.WriteCSS(rw)
		if err != nil {
			http.Error(rw, err.Error(), http.StatusInternalServerError)
		}
		return
	}
	_, modulePath, _ := strings.Cut(s.modulePath, "/")
	path, found := strings.CutPrefix(r.URL.Path, "/"+modulePath)
	if modulePath == "" {
		// stdlib hack: check if the first path component has a dot
		base, _, _ := strings.Cut(path, "/")
		found = !strings.ContainsRune(base, '.')
	}
	if !found {
		if s.gomodcache != nil && path != "/" {
			if err := s.servePackageFromGoModCache(rw, r, path); err != nil {
				slog.Info("could not serve from gomodcache", "path", path, "err", err)
			} else {
				return
			}
		}
		http.Redirect(rw, r, "/"+modulePath, http.StatusFound)
		return
	}

	if err := s.servePackage(rw, r, strings.Trim(path, "/")); err != nil {
		errorMsg := html.EscapeString(err.Error())
		// output some HTML for the livereload script to be injected
		rw.Header().Set("Content-Type", "text/html; charset=utf-8")
		rw.Header().Set("X-Content-Type-Options", "nosniff")
		statusCode, statusMsg := http.StatusInternalServerError, "502 Bad Gateway"
		if errors.Is(err, fs.ErrNotExist) {
			statusCode, statusMsg = http.StatusNotFound, "404 page not found"
		}
		rw.WriteHeader(statusCode)
		rw.Write([]byte(`<!DOCTYPE html><html><head><title>` + errorMsg + `</title></head><body><h1>` + statusMsg + `</h1><pre>`))
		rw.Write([]byte(errorMsg))
		return
	}
}

func (s server) servePackage(rw http.ResponseWriter, r *http.Request, path string) error {
	if s.useFallback || r.URL.Query().Has("fallback") {
		return s.servePackageDirectly(rw, path)
	}
	return s.servePackageFromRoot(rw, r, path)
}

func (s server) servePackageFromRoot(rw http.ResponseWriter, r *http.Request, path string) error {
	pkgs, err := pkgFromRoot(s.fsys, s.modulePath)
	if err != nil {
		return err
	}

	importPath := s.modulePath
	if path != "" {
		importPath += "/" + path
	}

	var target string
	for _, item := range pkgs {
		if item.ImportPath != importPath {
			if strings.HasSuffix(item.ImportPath, "/"+path) {
				// take the first import path with the right suffix and redirect to it
				if target == "" {
					// || len(item.ImportPath) < len(target) taking ths shortest could also be an option (but math/rand is shorter that crypto/rand)
					target = item.ImportPath
				}
			}
			continue
		}

		td, err := template.NewTemplateData(
			newBuildContext(item.FS),
			item.ModulePath,
			item.PackageChain,
			item.Directories,
			s.vcs,
			s.ref)
		if err != nil {
			return err
		}

		rw.Header().Set("Content-Type", "text/html; charset=utf-8")

		return s.renderer.Execute(rw, td)
	}

	if target != "" {
		target, _ = strings.CutPrefix(target, s.modulePath)
		http.Redirect(rw, r, target, http.StatusFound)
		return nil
	}

	return s.servePackageDirectly(rw, path)
}

func (s server) servePackageDirectly(rw http.ResponseWriter, path string) error {
	pkgChain := strings.Split(path, "/")
	if path == "" {
		path = "."
		pkgChain = nil
	}
	subFS, err := fs.Sub(s.fsys, path)
	if err != nil {
		return err
	}
	entries, err := fs.ReadDir(subFS, ".")
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			rw.WriteHeader(http.StatusNotFound)
			_, baseURL, _ := strings.Cut(s.modulePath, "/")
			if baseURL != "" {
				baseURL = "/" + baseURL
			}
			return s.renderer.NotFound(rw, baseURL, s.modulePath, path, s.ref)
		}
		return err
	}

	var directories []template.Directory
	start := time.Now()
	maxDelay := 50 * time.Millisecond
	// the loop below could be parallelized
	// but some max duration should be added as well
	for _, e := range entries {
		name := e.Name()
		if !e.IsDir() || name == "testdata" || strings.HasPrefix(name, ".") {
			continue
		}

		d := template.Directory{
			Name: name,
		}

		if time.Since(start) > maxDelay {
			d.Synopsis = "/* not enough time to compute synopsis */"
			directories = append(directories, d)
			continue
		}

		subPkgFS, err := fs.Sub(subFS, name)
		if err != nil {
			return err
		}
		d.Synopsis = template.Synopsis(newBuildContext(subPkgFS), s.modulePath, append(pkgChain, name))

		directories = append(directories, d)
	}

	td, err := template.NewTemplateData(
		newBuildContext(subFS),
		s.modulePath,
		pkgChain,
		directories,
		s.vcs,
		s.ref)
	if err != nil {
		return err
	}

	rw.Header().Set("Content-Type", "text/html; charset=utf-8")

	return s.renderer.Execute(rw, td)
}

func (s server) servePackageFromGoModCache(rw http.ResponseWriter, r *http.Request, path string) error {
	importPath := strings.Trim(path, "/")
	importPath, version, found := strings.Cut(importPath, "@")
	modulePath := importPath
	if found {
		var pkgPath string
		version, pkgPath, _ = strings.Cut(version, "/")
		if pkgPath != "" {
			importPath += "/" + pkgPath
		}
	}

	var listLines []byte
	var err error
	var root string
	for {
		root = "cache/download/" + escapeModulePath(modulePath) + "/@v/"
		listLines, err = fs.ReadFile(s.gomodcache, root+"list")
		if err == nil {
			break
		}

		i := strings.LastIndexByte(modulePath, '/')
		if i == -1 {
			if strings.ContainsRune(modulePath, '.') {
				rw.WriteHeader(http.StatusNotFound)
				return s.renderer.NotFound(rw, "", "[root]", importPath, version)
			}
			return fs.ErrNotExist
		}
		modulePath = modulePath[0:i]
	}

	var moduleVersion string
	for _, v := range strings.Split(string(listLines), "\n") {
		if version != "" {
			if v == version {
				moduleVersion = version
				break
			}
		} else if v != "" {
			moduleVersion = v
		}
	}
	if moduleVersion == "" {
		rw.WriteHeader(http.StatusNotFound)
		return s.renderer.NotFound(rw, "", "[root]", path, version)
	}

	zipFile, err := s.gomodcache.Open(root + moduleVersion + ".zip")
	if err != nil {
		return err
	}
	defer zipFile.Close()

	var size int64
	if seeker, ok := zipFile.(io.Seeker); !ok {
		err = fmt.Errorf("%T does not implement io.Seeker", zipFile)
	} else {
		size, err = seeker.Seek(0, io.SeekEnd)
	}
	if err != nil {
		return err
	}

	ra, ok := zipFile.(io.ReaderAt)
	if !ok {
		return fmt.Errorf("%T does not implement io.ReaderAt", zipFile)
	}
	zr, err := zip.NewReader(ra, size)
	if err != nil {
		return err
	}

	fsub, err := fs.Sub(zr, modulePath+"@"+moduleVersion)
	if err != nil {
		return err
	}

	pkgs, err := pkgFromRoot(fsub, modulePath)
	if err != nil {
		return err
	}

	for _, item := range pkgs {
		if item.ImportPath != importPath {
			continue
		}

		vcs := s.vcs
		if info, err := fs.ReadFile(s.gomodcache, root+moduleVersion+".info"); err == nil {
			vcs, _ = parseVCSFromModCacheInfo(info)
		}

		td, err := template.NewTemplateData(
			newBuildContext(item.FS),
			item.ModulePath,
			item.PackageChain,
			item.Directories,
			vcs,
			moduleVersion)
		if err != nil {
			return err
		}

		rw.Header().Set("Content-Type", "text/html; charset=utf-8")
		renderer := s.renderer
		renderer.DocSite = "/"
		renderer.BaseURL = "/" + item.ModulePath

		return renderer.Execute(rw, td)
	}

	rw.WriteHeader(http.StatusNotFound)
	return s.renderer.NotFound(rw, "/"+modulePath, modulePath, importPath, moduleVersion)
}

func parseVCSFromModCacheInfo(data []byte) (autodiscovery.VCS, error) {
	var info struct {
		Version string
		Time    time.Time
		Origin  struct {
			VCS  string
			URL  string
			Hash string
			Ref  string
		}
	}
	err := json.Unmarshal(data, &info)
	if err != nil {
		return autodiscovery.VCS{}, err
	}
	if vcs, err := autodiscovery.Infer(info.Origin.URL); err == nil {
		return vcs, nil
	}
	return autodiscovery.VCS{
		Kind: info.Origin.VCS,
		Clone: []string{
			info.Origin.URL,
		},
	}, nil
}

// copied from golang.org/x/mod/module#EscapePath
func escapeModulePath(s string) string {
	haveUpper := false
	for _, r := range s {
		if r == '!' || r >= utf8.RuneSelf {
			// this is invalid
			return s
		}
		if 'A' <= r && r <= 'Z' {
			haveUpper = true
		}
	}

	if !haveUpper {
		return s
	}

	var buf []byte
	for _, r := range s {
		if 'A' <= r && r <= 'Z' {
			buf = append(buf, '!', byte(r+'a'-'A'))
		} else {
			buf = append(buf, byte(r))
		}
	}
	return string(buf)
}
