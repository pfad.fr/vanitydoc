# SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
#
# SPDX-License-Identifier: CC0-1.0

dev:
	$(MAKE) --no-print-directory -j3 dev-devf dev-backend

dev-backend:
	pid="nonexisting" ; \
	(echo "initial generation" && inotifywait -m -e close_write --include "\.(go|gohtml|css)$$" -r .) | \
	while read -r path; do \
		echo "$$path and $$(timeout 0.1 cat | wc -l) other changes" ; \
		pkill -TERM -P $$pid ; \
		wait $$pid ; \
		( \
			go run . -addr=localhost:8989 -gomodcache=$$(go env GOMODCACHE) /usr/lib/go/src \
		) & \
		pid=$$! ; \
	done

dev-devf:
	inotifywait -m -e close_write --include "\.(is_running|js|css)$$" -r . | \
	COLUMNS=$$(tput cols) devf -addr :8080 http://localhost:8989

test: test-go-cover
	reuse lint

test-go-cover:
	go test -race -cover -coverpkg=./... -coverprofile $(COVERFOLDER)cover.all.out ./...
	@echo "Check if total coverage > 80%"
	@go tool cover -func $(COVERFOLDER)cover.all.out | tail -n1 | tee $(COVERFOLDER)cover.summary.out
	@grep -qE '([89][0-9](\.[0-9])?%)|100\.0%' $(COVERFOLDER)cover.summary.out
	@rm $(COVERFOLDER)cover.*.out

cover-html:
	@go tool cover -html=cover.all.out

help:
	go run . -h

doc:
	inotifywait -m -e close_write --include "\.go$$" -r . | \
		devf http://localhost:8181 -- go run . -addr localhost:8181

dev-testdata:
	inotifywait -m -e close_write -r example/testdata | devf example/testdata

systemd-socket-activate:
	go build -o result/vanitydoc .
	systemd-socket-activate -l 8080 ./result/vanitydoc -addr fd:3
