<!--
SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
SPDX-FileCopyrightText: 2013 The Go Authors

SPDX-License-Identifier: BSD-3-Clause
-->
{{ define "ROOT" }}
  <!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="utf-8" />
      {{ metaTags }}
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      {{ block "meta" $ }}
        <title>{{ .PageName }} - {{ .ModulePath }}</title>
      {{- end }}
    </head>
    <body>
      {{- block "header" $ }}{{ end }}
      {{- block "nav" $ }}
        <div class="module-nav sticky-header">
          <div>
            {{ range breadcrumbs -}}
              <a href="{{ .Href }}">{{ .Text }}</a>/
            {{- end -}}
            <a href="#top" title="Back to top"
              >{{ currentcrumb }} <span>↑</span></a
            >
          </div>
          <nav>
            {{- range $i, $section := sections -}}
              {{ if $i }}<span>|</span>&nbsp;{{ end }}<a
                href="{{ $section.Href }}"
                >{{ $section.Text }}</a
              >
            {{ end -}}
          </nav>
        </div>
      {{- end }}
      {{ template "Body" $ }}
      {{- block "footer" $ }}
        <div class="module-nav footer">
          <div>
            {{ range breadcrumbs -}}
              <a href="{{ .Href }}">{{ .Text }}</a>/<wbr />
            {{- end -}}
            <span>{{ currentcrumb }}</span>
          </div>
          {{ with .Ref }}<code>{{ . }}</code>{{ end }}
        </div>
        <footer>
          <nav>
            Generated with
            <a href="https://code.pfad.fr/vanitydoc/">vanitydoc{{with vanitydocVersion}} {{.}}{{end}}</a> &middot;
            <a href="https://go.dev">Go Language</a>
          </nav>
        </footer>
      {{- end }}
    </body>
  </html>
{{ end }}
