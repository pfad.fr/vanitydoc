// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
// SPDX-FileCopyrightText: 2013 The Go Authors
//
// SPDX-License-Identifier: BSD-3-Clause

// package template generates documentation for a given package.
package template

import (
	"bytes"
	"embed"
	"go/ast"
	"go/build"
	"go/doc"
	"go/doc/comment"
	"go/parser"
	"go/printer"
	"go/token"
	"io"
	"strings"

	"code.pfad.fr/vanitydoc/autodiscovery"
)

//go:embed templates
var templatesFS embed.FS

// Directory represents the sub-packages of a given package.
type Directory struct {
	Name     string
	Synopsis string
}

// NewTemplateData parses the provided pkgFS to prepare the template data.
func NewTemplateData(
	ctx build.Context,
	modulePath string,
	packageChain []string,
	directories []Directory,
	vcs autodiscovery.VCS,
	ref string,
) (TemplateData, error) {
	td := TemplateData{
		// Package:      filled below
		// FileSet:      filled below
		ModulePath:   modulePath,
		PackageChain: packageChain,
		Directories:  directories,
		VCS:          vcs,
		Ref:          ref,
	}
	var err error
	importPath := strings.Join(append([]string{modulePath}, packageChain...), "/")
	td.Package, td.FileSet, err = parseFiles(ctx, modulePath, importPath)

	return td, err
}

// Synopsis returns a cleaned version of the first sentence of the documentation
func Synopsis(ctx build.Context, modulePath string, packageChain []string) string {
	importPath := strings.Join(append([]string{modulePath}, packageChain...), "/")
	pkg, _, _ := parseFiles(ctx, modulePath, importPath)
	if pkg == nil {
		return ""
	}
	return pkg.Synopsis(pkg.Doc)
}

// TemplateData stores all the data needed to render the documentation templates.
type TemplateData struct {
	Package      *doc.Package
	FileSet      *token.FileSet
	ModulePath   string
	PackageChain []string
	Directories  []Directory
	VCS          autodiscovery.VCS
	Ref          string
}

// PageName returns the page name ("*** package" or "*** command" depending on the package name).
func (td TemplateData) PageName() string {
	s := td.Package.Name
	if s == "main" {
		parts := strings.Split(td.Package.ImportPath, "/")
		return parts[len(parts)-1] + " command"
	}
	return s + " package"
}

// AdjustedImportPath removes "std/" prefix (for showing the stdlib).
func (td TemplateData) AdjustedImportPath() string {
	s, _ := strings.CutPrefix(td.Package.ImportPath, "std/")
	return s
}

// WriteCSS writes the default CSS to the given writer.
func WriteCSS(w io.Writer) error {
	for _, name := range []string{"reseter.min.css", "style.css"} {
		r, err := templatesFS.Open("templates/" + name)
		if err != nil {
			return err
		}
		_, err = io.Copy(w, r)
		r.Close()
		if err != nil {
			return err
		}
		if _, err = w.Write([]byte("\n")); err != nil {
			return err
		}
	}
	return nil
}

func parseFiles(ctx build.Context, modulePath, importPath string) (*doc.Package, *token.FileSet, error) {
	list, err := ctx.ReadDir(".")
	if err != nil {
		return nil, nil, err
	}

	fileSet := token.NewFileSet()
	files := make([]*ast.File, 0)

	parseFile := func(name string) (*ast.File, error) {
		file, err := ctx.OpenFile(name)
		if err != nil {
			return nil, err
		}
		defer file.Close()
		// using parser.SkipObjectResolution (as recommended) triggers a panic in doc.NewFromFiles
		return parser.ParseFile(fileSet, name, file, parser.ParseComments)
	}

	for _, d := range list {
		name := d.Name()
		if d.IsDir() {
			continue
		}

		if match, err := ctx.MatchFile(".", name); err != nil {
			return nil, nil, err
		} else if !match {
			continue
		}

		file, err := parseFile(name)
		if err != nil {
			return nil, nil, err
		}
		files = append(files, file)
	}

	mode := doc.Mode(0)
	// hacky way to ensure that the documentation of "unexported" builtins is accessible
	if modulePath == "std" && importPath == "std/builtin" {
		mode |= doc.AllDecls
	}

	doc, err := doc.NewFromFiles(fileSet, files, importPath, mode)
	return doc, fileSet, err
}

type link struct{ Text, Href string }

func breadcrumbs(modulePath, baseURL string, packageChain []string, txtWithFullURL bool) []link {
	l := len(packageChain)
	if l == 0 {
		return nil
	}

	links := make([]link, 0, l)
	links = append(links, link{
		Href: baseURL + "/",
		Text: modulePath,
	})
	txt := modulePath
	for _, b := range packageChain[:l-1] {
		baseURL += "/" + b
		if txtWithFullURL {
			txt += "/" + b
		} else {
			txt = b
		}
		links = append(links, link{
			Href: baseURL + "/",
			Text: txt,
		})
	}
	return links
}

func docLinkUrl(docSite, currentDomain string, currentURL func(root string, dl *comment.DocLink) string) func(dl *comment.DocLink) string {
	currentIsNotDomain := !strings.ContainsRune(currentDomain, '.')
	return func(dl *comment.DocLink) string {
		if dl.ImportPath == "" { // within current package
			return currentURL("", dl)
		}
		if docSite == "/" { // serving gomodcache, all modules are hosted here
			return dl.DefaultURL(docSite)
		}

		var root string
		// assume that all modules with the same domain are hosted here
		if after, found := strings.CutPrefix(dl.ImportPath, currentDomain); found {
			root = after
		} else if domain, _, _ := strings.Cut(dl.ImportPath, "/"); currentIsNotDomain && !strings.ContainsRune(domain, '.') {
			// linking to a domain which does not look like a domain (e.g for "std" used by the stdlib)
			// assume doc is hosted here as well
			root = "/" + dl.ImportPath
		}

		if root != "" {
			return currentURL(root, dl)
		}
		return dl.DefaultURL(docSite)
	}
}

func newGoPrinter(fileSet *token.FileSet) goPrinter {
	return goPrinter{
		Config: printer.Config{
			Mode:     printer.UseSpaces | printer.TabIndent,
			Tabwidth: 8,
		},
		FileSet: fileSet,
	}
}

type goPrinter struct {
	Config  printer.Config
	FileSet *token.FileSet
}

func (gp goPrinter) PrettyPrintAST(node any) (string, error) {
	buf := &bytes.Buffer{}
	err := gp.Config.Fprint(buf, gp.FileSet, node)
	return buf.String(), err
}

func (gp goPrinter) ExampleTest(node any, comments []*ast.CommentGroup) (string, error) {
	s, err := gp.PrettyPrintAST(&printer.CommentedNode{
		Node:     node,
		Comments: comments,
	})
	if err != nil {
		return "", err
	}

	// remove surrounding braces
	// and first level of identation
	parts := strings.Split(s, "\n")
	if len(parts) < 2 {
		return s, nil
	}
	sb := strings.Builder{}
	for _, p := range parts[1 : len(parts)-1] {
		sb.WriteString(strings.TrimPrefix(p, "\t") + "\n")
	}
	return sb.String(), nil
}
