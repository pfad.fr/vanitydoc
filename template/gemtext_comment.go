// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
// SPDX-FileCopyrightText: 2013 The Go Authors
//
// SPDX-License-Identifier: BSD-3-Clause

package template

import (
	"bytes"
	"fmt"
	"go/doc/comment"
)

// adapted from /usr/lib/go/src/go/doc/comment/markdown.go

// An mdPrinter holds the state needed for printing a Doc as Markdown.
type mdPrinter struct {
	DocLinkURL    func(*comment.DocLink) string
	headingPrefix string

	raw     bytes.Buffer
	linkTxt bytes.Buffer
}

// Markdown returns a Markdown formatting of the Doc.
// See the [Printer] documentation for ways to customize the Markdown output.
func gemtext(d *comment.Doc, modulePath, docSite, currentDomain string) string {
	mp := &mdPrinter{
		DocLinkURL: docLinkUrl(docSite, currentDomain, func(root string, dl *comment.DocLink) string {
			if root == "" {
				// link relative to another part of the document
				// not support in gemtext. Links will not be outputted (see below)
				// https://gitlab.com/gemini-specification/gemini-text/-/issues/3
				switch {
				case dl.Name == "":
					return root
				case dl.Recv != "":
					return root + "#" + dl.Recv + "." + dl.Name
				default:
					return root + "#" + dl.Name
				}
			}
			return root
		}),
		headingPrefix: "## ",
	}

	var out bytes.Buffer
	for i, x := range d.Content {
		if i > 0 {
			out.WriteByte('\n')
		}
		mp.block(&out, x)
	}
	return out.String()
}

// block prints the block x to out.
func (p *mdPrinter) block(out *bytes.Buffer, x comment.Block) {
	var links []*comment.LinkDef
	switch x := x.(type) {
	default:
		fmt.Fprintf(out, "?%T", x)

	case *comment.Paragraph:
		links = p.text(out, x.Text)
		out.WriteString("\n")

	case *comment.Heading:
		out.WriteString(p.headingPrefix)
		links = p.text(out, x.Text)
		out.WriteString("\n")

	case *comment.Code:
		out.WriteString("```\n")
		out.WriteString(x.Text)
		out.WriteString("```\n")

	case *comment.List:
		loose := x.BlankBetween()
		for i, item := range x.Items {
			if i > 0 && loose {
				out.WriteString("\n")
			}
			if n := item.Number; n != "" {
				out.WriteString(" ")
				out.WriteString(n)
				out.WriteString(". ")
			} else {
				out.WriteString("* ")
			}
			for i, blk := range item.Content {
				const fourSpace = "    "
				if i > 0 {
					out.WriteString("\n" + fourSpace)
				}
				links = append(links, p.text(out, blk.(*comment.Paragraph).Text)...)
				out.WriteString("\n")
			}
		}
	}
	for _, l := range links {
		if len(l.URL) > 0 && l.URL[0] == '#' {
			// fragments are not supported in gemtext
			// https://gitlab.com/gemini-specification/gemini-text/-/issues/3
			continue
		}
		out.WriteString("=> ")
		out.WriteString(l.URL)
		out.WriteByte(' ')
		out.WriteString(l.Text)
		out.WriteByte('\n')
	}
}

// text prints the text sequence x to out.
func (p *mdPrinter) text(out *bytes.Buffer, x []comment.Text) []*comment.LinkDef {
	p.raw.Reset()
	links := p.rawText(&p.raw, x)
	line := bytes.TrimSpace(p.raw.Bytes())
	if len(line) == 0 {
		return links
	}
	switch line[0] {
	case '*', '#':
		// Escape what would be the start of an unordered list or heading.
		out.WriteByte(' ')
	}
	out.Write(line)
	return links
}

// rawText prints the text sequence x to out,
// without worrying about escaping characters
// that have special meaning at the start of a Markdown line.
func (p *mdPrinter) rawText(out *bytes.Buffer, x []comment.Text) []*comment.LinkDef {
	var comments []*comment.LinkDef
	for _, t := range x {
		switch t := t.(type) {
		case comment.Plain:
			p.escape(out, string(t))
		case comment.Italic:
			out.WriteString("*")
			p.escape(out, string(t))
			out.WriteString("*")
		case *comment.Link:
			p.linkTxt.Reset()
			p.rawText(&p.linkTxt, t.Text)
			comments = append(comments, &comment.LinkDef{
				Text: p.linkTxt.String(),
				URL:  t.URL,
				Used: true,
			})
			out.WriteByte('[')
			p.linkTxt.WriteTo(out)
			out.WriteByte(']')
			// out.WriteString(t.URL)
		case *comment.DocLink:
			url := p.DocLinkURL(t)
			if url == "" {
				p.rawText(out, t.Text)
			} else {
				p.linkTxt.Reset()
				p.rawText(&p.linkTxt, t.Text)
				comments = append(comments, &comment.LinkDef{
					Text: p.linkTxt.String(),
					URL:  url,
					Used: true,
				})
				out.WriteByte('[')
				p.linkTxt.WriteTo(out)
				out.WriteByte(']')
				// out.WriteString(url)
			}
		}
	}
	return comments
}

// escape prints s to out as plain text,
// escaping special characters to avoid being misinterpreted
// as Markdown markup sequences.
func (p *mdPrinter) escape(out *bytes.Buffer, s string) {
	start := 0
	for i := 0; i < len(s); i++ {
		switch s[i] {
		case '\n':
			// Turn all \n into spaces, for a few reasons:
			//   - Avoid introducing paragraph breaks accidentally.
			//   - Avoid the need to reindent after the newline.
			//   - Avoid problems with Markdown renderers treating
			//     every mid-paragraph newline as a <br>.
			out.WriteString(s[start:i])
			out.WriteByte(' ')
			start = i + 1
			continue
		}
	}
	out.WriteString(s[start:])
}
