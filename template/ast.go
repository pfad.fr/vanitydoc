// SPDX-FileCopyrightText: 2024 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

package template

import (
	"bytes"
	"fmt"
	"go/ast"
	"go/doc/comment"
	"go/token"
	"html"
	"html/template"
	"io"
	"strings"
	"text/tabwriter"
)

type astFormatter struct {
	Fallback   func(node any) (string, error)
	DocLinkURL func(link *comment.DocLink) string
}

var anchorLinkTemplate = template.Must(template.New("name").Parse(`<a href="{{.Href}}">{{.Text}}</a>`))

func (af astFormatter) formatExpr(w io.Writer, expr ast.Expr, initialIndent int, docLink bool) error {
	// w.Write([]byte(`<abbr title="` + strings.TrimPrefix(fmt.Sprintf("%T", expr), "*ast.") + `" class="hover-debug">`))
	// defer func() { w.Write([]byte(`</abbr>`)) }()

	_, insideTabWriter := w.(*tabwriter.Writer)

	switch t := expr.(type) {
	case *ast.Ident:
		// int, float, package name ...

		data := struct {
			Href, Text string
		}{
			Text: t.Name,
		}
		if docLink {
			if t.Obj == nil {
				data.Href = af.DocLinkURL(&comment.DocLink{
					ImportPath: "builtin",
					Name:       t.Name,
				})
				// data.Text += "@" + fmt.Sprintf("%#v", t)
			} else if t.Obj.Kind == ast.Pkg {
				if is, ok := t.Obj.Decl.(*ast.ImportSpec); ok {
					data.Href = af.DocLinkURL(&comment.DocLink{
						ImportPath: strings.Trim(is.Path.Value, `"`),
					})
				}
			} else if t.IsExported() && t.Obj.Kind == ast.Typ {
				if _, isGeneric := t.Obj.Decl.(*ast.Field); !isGeneric {
					data.Href = "#" + t.Name
				}
			}
		}
		// data.Text = t.Name + "@" + fmt.Sprintf("%#v", t.Obj.Decl)
		if data.Href != "" {
			err := anchorLinkTemplate.Execute(w, data)
			if err != nil {
				return err
			}
		} else {
			w.Write([]byte(t.Name))
		}
	case *ast.SelectorExpr:
		// pkg.Name
		var identObj *ast.Object
		if ident, ok := t.X.(*ast.Ident); ok && docLink && ident.Obj != nil {
			identObj = ident.Obj
		}
		err := af.formatExpr(w, t.X, initialIndent, identObj != nil)
		if err != nil {
			return err
		}
		w.Write([]byte{'.'})

		data := struct {
			Href, Text string
		}{
			Text: t.Sel.Name,
		}
		if identObj != nil {
			if is, ok := identObj.Decl.(*ast.ImportSpec); ok {
				data.Href = af.DocLinkURL(&comment.DocLink{
					ImportPath: strings.Trim(is.Path.Value, `"`),
					Name:       t.Sel.Name,
				})
			}
		}
		if data.Href != "" {
			err := anchorLinkTemplate.Execute(w, data)
			if err != nil {
				return err
			}
		} else {
			w.Write([]byte(t.Sel.Name))
		}

	case *ast.InterfaceType:
		w.Write([]byte("interface"))
		if len(t.Methods.List) == 0 {
			if t.Incomplete {
				w.Write([]byte(` {<span class="comment">/* contains filtered or unexported methods */</span>}`))
			} else {
				w.Write([]byte("{}"))
			}
			return nil
		}

		if t.Methods.Opening > t.Interface+token.Pos(len("interface")) {
			w.Write([]byte{' '})
		}
		w.Write([]byte("{"))
		isMultiline, err := af.formatFieldList(w, t.Methods, initialIndent+1, fieldListOpts{
			IsIncomplete: t.Incomplete,
			IsInterface:  true,
		})
		if err != nil {
			return err
		}

		if isMultiline {
			w.Write([]byte{'\n'})
		} else if len(t.Methods.List) > 0 {
			w.Write([]byte{' '})
		}
		w.Write([]byte("}"))

	case *ast.StructType:
		w.Write([]byte("struct"))
		if len(t.Fields.List) == 0 {
			if t.Incomplete {
				w.Write([]byte(` {<span class="comment">/* contains filtered or unexported fields */</span>}`))
			} else {
				w.Write([]byte("{}"))
			}
			return nil
		}

		if t.Fields.Opening > t.Struct+token.Pos(len("struct")) {
			w.Write([]byte{' '})
		}
		w.Write([]byte{'{'})

		var buf io.Writer = w
		if insideTabWriter {
			buf = &bytes.Buffer{}
		}
		isMultiline, err := af.formatFieldList(buf, t.Fields, initialIndent+1, fieldListOpts{
			IsIncomplete: t.Incomplete,
			IsInterface:  false,
		})
		if err != nil {
			return err
		}
		if insideTabWriter {
			w.Write([]byte{tabwriter.Escape})
			buf.(*bytes.Buffer).WriteTo(w)
			w.Write([]byte{tabwriter.Escape})
		}
		if isMultiline {
			w.Write([]byte{'\n'})
			for i := 0; i < initialIndent; i++ {
				w.Write([]byte{'\t'})
			}
		} else if len(t.Fields.List) > 0 {
			w.Write([]byte{' '})
		}
		w.Write([]byte{'}'})

	case *ast.MapType:
		w.Write([]byte("map["))
		if err := af.formatExpr(w, t.Key, initialIndent, docLink); err != nil {
			return err
		}
		w.Write([]byte{']'})
		if err := af.formatExpr(w, t.Value, initialIndent, docLink); err != nil {
			return err
		}

	case *ast.FuncType:
		if t.Func.IsValid() {
			var next token.Pos
			if t.TypeParams != nil {
				next = t.TypeParams.Opening
			} else if t.Params != nil {
				next = t.Params.Opening
			}

			if t.Func+token.Pos(len("func")) == next {
				w.Write([]byte("func"))
			}
		}
		if t.TypeParams != nil {
			w.Write([]byte{'['})
			af.formatFieldListNonVertical(w, t.TypeParams, initialIndent+1, docLink)
			w.Write([]byte{']'})
		}
		w.Write([]byte{'('})
		if t.Params != nil && len(t.Params.List) > 0 {
			af.formatFieldListNonVertical(w, t.Params, initialIndent+1, docLink)
		}
		w.Write([]byte{')'})
		if t.Results != nil {
			w.Write([]byte{' '})
			if t.Results.Opening.IsValid() {
				w.Write([]byte{'('})
			}
			if err := af.formatFieldListNonVertical(w, t.Results, initialIndent+1, docLink); err != nil {
				return err
			}
			if t.Results.Opening.IsValid() {
				w.Write([]byte{')'})
			}
		}

	case *ast.BasicLit:
		w.Write([]byte(html.EscapeString(t.Value)))

	case *ast.CompositeLit:
		if t.Type != nil {
			if err := af.formatExpr(w, t.Type, initialIndent, docLink); err != nil {
				return err
			}
		}

		w.Write([]byte{'{'})
		previous, err := af.formatExprs(w, t.Elts, t.Lbrace, initialIndent, docLink)
		if err != nil {
			return err
		}
		if t.Rbrace > previous+1 {
			if len(t.Elts) > 0 {
				w.Write([]byte{',', '\n'})
				for i := 0; i < initialIndent; i++ {
					w.Write([]byte{'\t'})
				}
				if t.Incomplete {
					w.Write([]byte("\t" + `<span class="comment">// contains filtered or unexported fields</span>` + "\n"))
					for i := 0; i < initialIndent; i++ {
						w.Write([]byte{'\t'})
					}
				}
			} else if t.Incomplete {
				w.Write([]byte(`<span class="comment">/* contains filtered or unexported fields */</span>`))
			}
		}
		w.Write([]byte{'}'})

	case *ast.ArrayType:
		w.Write([]byte{'['})
		if t.Len != nil {
			if err := af.formatExpr(w, t.Len, initialIndent, docLink); err != nil {
				return err
			}
		}
		w.Write([]byte{']'})
		if err := af.formatExpr(w, t.Elt, initialIndent, docLink); err != nil {
			return err
		}

	case *ast.CallExpr:
		if err := af.formatExpr(w, t.Fun, initialIndent, docLink); err != nil {
			return err
		}
		w.Write([]byte{'('})
		previous := t.Lparen
		for i, e := range t.Args {
			if i > 0 {
				w.Write([]byte{','})
			}
			if e.Pos() > previous+2 {
				w.Write([]byte{'\n', '\t'})
			} else if i > 0 {
				w.Write([]byte{' '})
			}

			if err := af.formatExpr(w, e, initialIndent, docLink); err != nil {
				return err
			}
			previous = e.End()
		}
		if t.Ellipsis.IsValid() {
			w.Write([]byte{'.', '.', '.'})
			previous = t.Ellipsis + 3
		}
		if t.Rparen > previous+1 {
			w.Write([]byte{',', '\n'})
		}
		w.Write([]byte{')'})

	case *ast.Ellipsis:
		w.Write([]byte("..."))
		if t.Elt != nil {
			if err := af.formatExpr(w, t.Elt, initialIndent, docLink); err != nil {
				return err
			}
		}

	case *ast.ChanType:
		if t.Dir == ast.RECV {
			w.Write([]byte("&lt;-"))
		}
		w.Write([]byte("chan"))
		if t.Dir == ast.SEND {
			w.Write([]byte("&lt;-"))
		}
		w.Write([]byte{' '})
		if err := af.formatExpr(w, t.Value, initialIndent, docLink); err != nil {
			return err
		}

	case *ast.IndexExpr:
		if err := af.formatExpr(w, t.X, initialIndent, docLink); err != nil {
			return err
		}
		w.Write([]byte{'['})
		if err := af.formatExpr(w, t.Index, initialIndent, docLink); err != nil {
			return err
		}
		w.Write([]byte{']'})

	case *ast.IndexListExpr:
		if err := af.formatExpr(w, t.X, initialIndent, docLink); err != nil {
			return err
		}
		w.Write([]byte{'['})
		if _, err := af.formatExprs(w, t.Indices, t.Lbrack, initialIndent, docLink); err != nil {
			return err
		}
		w.Write([]byte{']'})

	case *ast.KeyValueExpr:
		if err := af.formatExpr(w, t.Key, initialIndent, false); err != nil {
			return err
		}
		w.Write([]byte{':', '\t'})
		if err := af.formatExpr(w, t.Value, initialIndent, docLink); err != nil {
			return err
		}

	case *ast.UnaryExpr:
		w.Write([]byte(html.EscapeString(t.Op.String())))
		if err := af.formatExpr(w, t.X, initialIndent, docLink); err != nil {
			return err
		}

	case *ast.StarExpr:
		w.Write([]byte{'*'})
		if err := af.formatExpr(w, t.X, initialIndent, docLink); err != nil {
			return err
		}

	case *ast.BinaryExpr:
		op := t.Op.String()
		if err := af.formatExpr(w, t.X, initialIndent, docLink); err != nil {
			return err
		}
		if spacing := t.OpPos - t.X.End(); spacing > 0 {
			w.Write([]byte{' '})
		}

		w.Write([]byte(op))

		if spacing := t.Y.Pos() - (t.OpPos + token.Pos(len(op))); spacing > 1 {
			w.Write([]byte{'\n'})
			for i := 0; i < initialIndent+1; i++ {
				w.Write([]byte{'\t'})
			}
		} else if spacing > 0 {
			w.Write([]byte{' '})
		}
		if err := af.formatExpr(w, t.Y, initialIndent, docLink); err != nil {
			return err
		}

	case *ast.ParenExpr:
		w.Write([]byte{'('})
		if err := af.formatExpr(w, t.X, initialIndent, docLink); err != nil {
			return err
		}
		w.Write([]byte{')'})

	case *ast.SliceExpr:
		if err := af.formatExpr(w, t.X, initialIndent, docLink); err != nil {
			return err
		}
		w.Write([]byte{'['})
		if t.Low != nil {
			if err := af.formatExpr(w, t.Low, initialIndent, docLink); err != nil {
				return err
			}
		}
		w.Write([]byte{':'})
		if t.High != nil {
			if err := af.formatExpr(w, t.High, initialIndent, docLink); err != nil {
				return err
			}
		}
		if t.Slice3 {
			w.Write([]byte{':'})
			if t.Max != nil {
				if err := af.formatExpr(w, t.Max, initialIndent, docLink); err != nil {
					return err
				}
			}
		}
		w.Write([]byte{']'})

	case *ast.TypeAssertExpr:
		if err := af.formatExpr(w, t.X, initialIndent, docLink); err != nil {
			return err
		}
		w.Write([]byte(".("))
		if t.Type == nil {
			w.Write([]byte("type"))
		} else {
			if err := af.formatExpr(w, t.Type, initialIndent, docLink); err != nil {
				return err
			}
		}
		w.Write([]byte{')'})

	case *ast.FuncLit:
		if err := af.formatExpr(w, t.Type, initialIndent, docLink); err != nil {
			return err
		}
		w.Write([]byte{' '})
		s, err := af.Fallback(t.Body)
		if err != nil {
			return err
		}
		w.Write([]byte(html.EscapeString(s)))

	case *ast.BadExpr:
		w.Write([]byte(`<span class="comment">/* ast.BadExpr */</span>`))

	default:
		return fmt.Errorf("unsupported Expr: %T", t)
	}
	return nil
}

func (af astFormatter) formatExprs(w io.Writer, exprs []ast.Expr, previous token.Pos, initialIndent int, docLink bool) (token.Pos, error) {
	for i, e := range exprs {
		if i > 0 {
			w.Write([]byte{','})
		}
		if e.Pos() > previous+2 {
			if i > 0 && e.Pos() > previous+3 {
				w.Write([]byte{'\n'})
			}
			w.Write([]byte{'\n'})

			for i := 0; i < initialIndent+1; i++ {
				w.Write([]byte{'\t'})
			}

		} else if i > 0 {
			w.Write([]byte{' '})
		}
		if err := af.formatExpr(w, e, initialIndent+1, docLink); err != nil {
			return previous, err
		}
		previous = e.End()
	}
	return previous, nil
}

func (af astFormatter) formatGenDecl(w io.Writer, decl *ast.GenDecl) error {
	for _, s := range decl.Specs {
		switch v := s.(type) {
		case *ast.TypeSpec:
			w.Write([]byte("type " + v.Name.String()))
			if v.TypeParams != nil {
				w.Write([]byte{'['})
				if err := af.formatFieldListNonVertical(w, v.TypeParams, 1, true); err != nil {
					return err
				}
				w.Write([]byte{']'})
			}
			w.Write([]byte{' '})
			if err := af.formatExpr(w, v.Type, 0, true); err != nil {
				return err
			}
		default:
			return fmt.Errorf("unsupported type inside formatTypeSpec: %T", v)
		}
	}

	return nil
}

func (af astFormatter) formatFuncDecl(w io.Writer, decl *ast.FuncDecl, docLink bool) error {
	w.Write([]byte("func "))
	if decl.Recv != nil {
		w.Write([]byte{'('})
		if err := af.formatFieldListNonVertical(w, decl.Recv, 0, docLink); err != nil {
			return err
		}
		w.Write([]byte(") "))
	}

	w.Write([]byte(decl.Name.String()))
	return af.formatExpr(w, decl.Type, 0, docLink)
}

func (af astFormatter) formatFieldListNonVertical(w io.Writer, fl *ast.FieldList, initialIndent int, docLink bool) error {
	newline := make([]byte, 0, initialIndent+1)
	newline = append(newline, '\n')
	for i := 0; i < initialIndent; i++ {
		newline = append(newline, '\t')
	}

	previous := fl.Opening
	for i, f := range fl.List {
		if i > 0 {
			w.Write([]byte{','})
			if f.Pos() > previous+2 {
				w.Write(newline)
			} else {
				w.Write([]byte{' '})
			}
		} else if previous.IsValid() && f.Pos() > previous+1 {
			w.Write(newline)
		}

		if f.Doc != nil {
			return fmt.Errorf("unexpected Doc in formatFieldListNonVertical: %v", f.Doc)
		}

		for i, name := range f.Names {
			if i > 0 {
				w.Write([]byte{','})
				if name.Pos() > previous+2 {
					w.Write(newline)
				} else {
					w.Write([]byte{' '})
				}
			}
			w.Write([]byte(name.Name))
			previous = name.End()
		}
		if len(f.Names) > 0 {
			w.Write([]byte{' '})
		}
		if err := af.formatExpr(w, f.Type, 0, docLink); err != nil {
			return err
		}

		previous = f.End()
		if f.Comment != nil {
			return fmt.Errorf("unexpected Comment in formatFieldListNonVertical: %v", f.Comment)
		}
	}
	if fl.Closing.IsValid() && fl.Closing > previous {
		w.Write([]byte{',', '\n'})
		for i := 0; i < initialIndent-1; i++ {
			w.Write([]byte{'\t'})
		}
	}
	return nil
}

type fieldListOpts struct {
	IsIncomplete bool
	IsInterface  bool
}

func (af astFormatter) formatFieldList(w io.Writer, fl *ast.FieldList, initialIndent int, opts fieldListOpts) (bool, error) {
	tw := tabwriter.NewWriter(w, 1, 4, 1, ' ', tabwriter.FilterHTML|tabwriter.StripEscape|tabwriter.TabIndent)

	previous := fl.Opening + 1
	isMultiline := false
	for _, f := range fl.List {
		isOnNewLine := false
		// tw.Write([]byte(fmt.Sprintf("%d-%d (%d)", previous, f.Pos(), initialIndent)))

		if previous.IsValid() {
			current := f.Pos()
			if f.Doc != nil {
				current = f.Doc.Pos()
			}

			expectedHorizontalSpacing := token.Pos(max(2, initialIndent))
			// add "cosmetic" vertical spacing
			if current >= previous+expectedHorizontalSpacing {
				if current >= previous+1+expectedHorizontalSpacing && isMultiline {
					// additional newline if there is an empty newline between the fields
					// does not work well with space indentation: https://codeberg.org/pfad.fr/vanitydoc/issues/17
					tw.Write([]byte{'\n'})
				}
				tw.Write([]byte{'\n'})
				isOnNewLine = true
				isMultiline = true
			}
		}

		if f.Doc != nil {
			// top comment
			for _, c := range f.Doc.List {
				for i := 0; i < initialIndent; i++ {
					tw.Write([]byte{'\t'})
				}
				tw.Write([]byte(`<span class="comment">` + html.EscapeString(c.Text) + "</span>"))
				tw.Write([]byte{'\n'})
				isMultiline = true
			}
		}

		if isOnNewLine {
			for i := 0; i < initialIndent; i++ {
				tw.Write([]byte{'\t'})
			}
		} else if f.Pos() > previous {
			tw.Write([]byte{' '})
		}
		previous = f.End()
		for i, name := range f.Names {
			if i > 0 {
				tw.Write([]byte{',', ' '})
			}
			tw.Write([]byte(name.Name))
		}

		if !opts.IsInterface && len(f.Names) > 0 {
			if isMultiline {
				tw.Write([]byte{'\t'})
			} else {
				tw.Write([]byte{' '})
			}
		}
		if err := af.formatExpr(tw, f.Type, initialIndent, true); err != nil {
			return isMultiline, err
		}
		space := byte('\t')
		if _, ok := f.Type.(*ast.StructType); ok {
			space = ' '
		}
		if f.Tag != nil {
			// struct tag
			tw.Write([]byte{space})
			tw.Write([]byte(html.EscapeString(f.Tag.Value)))
			previous = f.Tag.End()
			space = ' '
		}

		if f.Comment != nil {
			// trailing comment
			tw.Write([]byte{space})
			tw.Write([]byte(`<span class="comment">// ` + html.EscapeString(strings.TrimSpace(f.Comment.Text())) + `</span>`))
			previous = f.Comment.End()
		}
	}
	if opts.IsIncomplete {
		tw.Write([]byte{'\n'})
		for i := 0; i < initialIndent; i++ {
			tw.Write([]byte{'\t'})
		}
		unexported := "fields"
		if opts.IsInterface {
			unexported = "methods"
		}
		tw.Write([]byte(`<span class="comment">// contains filtered or unexported ` + unexported + "</span>"))
		isMultiline = true
	}

	return isMultiline, tw.Flush()
}
